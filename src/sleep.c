/*
 * sleep.c
 *
 *  Created on: Jan 13, 2014
 *      Author: jenswilly
 *
 *  Based on code from AN11027
 */

#include "LPC11Uxx.h"

void deep_power_down()
{
	/* 3.9.6.2 Programming Deep power-down mode
	 *
	 * The following steps must be performed to enter Deep power-down mode:
	 * 1. Pull the WAKEUP pin externally HIGH.
	 * 2. Ensure that bit 3 in the PCON register (Table 53) is cleared.
	 * 3. Write 0x3 to the PD bits in the PCON register (see Table 53).
	 * 4. Store data to be retained in the general purpose registers (Section 4.3.2).
	 * 5. Write one to the SLEEPDEEP bit in the ARM Cortex-M0 SCR register.
	 * 6. Use the ARM WFI instruction.
	 */

	// 1. WAKEUP pin (PIO0.16/pin 40) is pulled up externally.
	// 2. Bit 3 in the PCON register can only be cleared by power-on reset. But as it should never be set, it will be cleared now.

	// 3. Write 0x3 to the PD bits in the PCON register (see Table 53).
	LPC_PMU->PCON |= 0x03;	// Specify DPDEN to power control register

	// 4. Store data to be retained in the general purpose registers (Section 4.3.2).
	// Not applicable

	// 5. Write one to the SLEEPDEEP bit in the ARM Cortex-M0 SCR register.
	SCB->SCR |=	(1<<2);		//Set SLEEPDEEP bit

	// Enable IRC before deep power-down mode
	// LPC_SYSCON->PDRUNCFG &= ~((1<<0) | (1<<1));	// Not necessary. Refer to above procedure

	// Reconfigure the IOs
	// config_ios();	// Not necessary, since manual state that "all functional pins are tri-stated expect WAKEUP pin"

	// Use the ARM WFI instruction.
	__WFI();

	NVIC_SystemReset();
}

void config_ios(void){

	// Configure all used IOs as GPIO w/o pull-up & pull-down resistors
	LPC_IOCON->RESET_PIO0_0 = 0x81;		// GPIO
	LPC_IOCON->PIO0_1 = 0x80;
	LPC_IOCON->PIO0_2 = 0x80;
	LPC_IOCON->PIO0_3 = 0x80;
	LPC_IOCON->PIO0_4 = 0x80;
	LPC_IOCON->PIO0_5 = 0x80;
	LPC_IOCON->PIO0_6 = 0x80;
	LPC_IOCON->PIO0_7 = 0x80;
	LPC_IOCON->PIO0_8 = 0x80;
	LPC_IOCON->PIO0_9 = 0x80;
	LPC_IOCON->SWCLK_PIO0_10 = 0x81;	// GPIO
	LPC_IOCON->TDI_PIO0_11 = 0x81;		// GPIO, digital mode
	LPC_IOCON->TMS_PIO0_12 = 0x81;		// GPIO
	LPC_IOCON->TDO_PIO0_13 = 0x81;		// GPIO, digital mode
	LPC_IOCON->TRST_PIO0_14 = 0x81;		// GPIO, digital mode
	LPC_IOCON->SWDIO_PIO0_15 = 0x81;	// GPIO, digital mode
	LPC_IOCON->PIO0_16 = 0x80;			// Digital mode
	LPC_IOCON->PIO0_17 = 0x80;
	LPC_IOCON->PIO0_18 = 0x80;
	LPC_IOCON->PIO0_19 = 0x80;
	LPC_IOCON->PIO0_20 = 0x80;
	LPC_IOCON->PIO0_21 = 0x80;
	LPC_IOCON->PIO0_22 = 0x80;			// Digital mode
	LPC_IOCON->PIO0_23 = 0x80;			// Digital mode
	LPC_IOCON->PIO1_0 = 0x80;
	LPC_IOCON->PIO1_1 = 0x80;
	LPC_IOCON->PIO1_2 = 0x80;
	LPC_IOCON->PIO1_3 = 0x80;
	LPC_IOCON->PIO1_4 = 0x80;
	LPC_IOCON->PIO1_5 = 0x80;
	LPC_IOCON->PIO1_6 = 0x80;
	LPC_IOCON->PIO1_7 = 0x80;
	LPC_IOCON->PIO1_8 = 0x80;
	LPC_IOCON->PIO1_9 = 0x80;
	LPC_IOCON->PIO1_10 = 0x80;
	LPC_IOCON->PIO1_11 = 0x80;
	LPC_IOCON->PIO1_12 = 0x80;
	LPC_IOCON->PIO1_13 = 0x80;
	LPC_IOCON->PIO1_14 = 0x80;
	LPC_IOCON->PIO1_15 = 0x80;
	LPC_IOCON->PIO1_16 = 0x80;			// #WAKEUP regardless of how the pin is configured
	LPC_IOCON->PIO1_17 = 0x80;
	LPC_IOCON->PIO1_18 = 0x80;
	LPC_IOCON->PIO1_19 = 0x80;
	LPC_IOCON->PIO1_20 = 0x80;
	LPC_IOCON->PIO1_21 = 0x80;
	LPC_IOCON->PIO1_22 = 0x80;
	LPC_IOCON->PIO1_23 = 0x80;
	LPC_IOCON->PIO1_24 = 0x80;
	LPC_IOCON->PIO1_25 = 0x80;
	LPC_IOCON->PIO1_26 = 0x80;
	LPC_IOCON->PIO1_27 = 0x80;
	LPC_IOCON->PIO1_28 = 0x80;
	LPC_IOCON->PIO1_29 = 0x80;
	LPC_IOCON->PIO1_30 = 0x80;
	LPC_IOCON->PIO1_31 = 0x80;

	// Set all as outputs except #WAKEUP pin
	LPC_GPIO->DIR[ 0 ] = 0xFFFFFFFF;
	LPC_GPIO->DIR[ 1 ] = 0xFFFFFFFF;

	// All outputs low
	LPC_GPIO->PIN[ 0 ] = 0;
	LPC_GPIO->PIN[ 1 ] = 0;
}
