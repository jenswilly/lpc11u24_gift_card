/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include <string.h>
#include "LPC11Uxx.h"
#include <cr_section_macros.h>
#include "drivers/systick.h"
#include "drivers/i2c.h"
#include "drivers/eeprom.h"
#include "LCD.h"
#include "status_led.h"
#include "sleep.h"
#include "MMA8452Q.h"
#include "mw_usbd_rom_api.h"
#include "usb_cdc.h"

#define BACKLIGHT_DELAY 15000	// Backlight on duration in ms

// Function prototypes
void parseCommand( void );
void USB_CDC_print( char* string );

// RX buffer and state definitions
enum
{
	StateIdle,
	StateCmdReceived,	// Entire command received: go parse it
};

#define CMD_BUF_SIZE 200
uint8_t commandBuffer[CMD_BUF_SIZE];
uint8_t sendBuffer[ 34 ];	// 33 bytes is enough for two lines of 16 bytes plus a newline separator plus terminating zero.
volatile uint16_t commandBufferPtr;
volatile uint16_t state;
char eeprom_signature_char = '@';

int main(void)
{
	uint8_t wakeup_from_powerdown;
	char eeprom_signature;

	// Initialize timing and enable GPIO/IOCON clocks
	SystemCoreClockUpdate();
	SysTick_Config( SystemCoreClock / 1000 );			// For a SysTick frequency of 1000 Hz. Each tick is 1 ms
	LPC_SYSCON->SYSAHBCLKCTRL |= (1>> 6) | (1>> 16);	// Power up clocks for GPIO and IOCON

	// Initialize peripherals
	init_leds();								// Init debugging LED on PIO0_2

	// Did we wake up from deep power-down?
	if( LPC_PMU->PCON & (1<<11) )
	{
		// Yes we did: set var and clear flag
		wakeup_from_powerdown = 1;
		LPC_PMU->PCON |= (1<<11);
	}
	else
		// No: cold start
		wakeup_from_powerdown = 0;

	// Read text from EEPROM into RAM
	// See if this is first launch. If anyting has been written to the EEPROM, offset 0 will contain the character @
	readEEPROM( (uint8_t*)0, (uint8_t*)&eeprom_signature, 1 );
	if( eeprom_signature != eeprom_signature_char )
	{
		// EEPROM not initialized: do it now
		memset( line1, '>', 16 );	// Line 1: >>>>>>>>>>>>>>>>
		memset( line2, '<', 16 );	// Line 2: <<<<<<<<<<<<<<<<
		writeEEPROM( (uint8_t*)1, (uint8_t*)line1, 16 );
		writeEEPROM( (uint8_t*)17, (uint8_t*)line2, 16 );

		// Write EEPROM signature
		writeEEPROM( (uint8_t*)0, (uint8_t*)&eeprom_signature_char, 1 );
	}
	else
	{
		// EEPROM already written: read texts into RAM
		readEEPROM( (uint8_t*)1, (uint8_t*)line1, 16 );
		readEEPROM( (uint8_t*)17, (uint8_t*)line2, 16 );
	}

	// USB and state initialization
	USB_CDC_init();
	state = StateIdle;
	memset( commandBuffer, CMD_BUF_SIZE, 0 );
	commandBufferPtr = 0;

	// Init GPIO output for LCD backlight FET and turn on
	LPC_IOCON->PIO1_13 = 0x80;
	LPC_GPIO->DIR[ 1 ] |= (1<< 13);
	LPC_GPIO->SET[ 1 ] = (1<< 13);

	// Initialize (or re-initialize) I2C
	I2CInit( I2CMASTER );						// Init I2C

	// The following should only happen on power-up and not on wakeup from deep power-down since the LCD and acceleromter will already be configured.
	if( !wakeup_from_powerdown )
	{
		SysTick_Delay( 100 );					// Wait a bit for all devices to be ready
		ST7032_init( ST7032_InitMode_3V3_v2 );	// Init LCD (3.3 V, version 2)
		MMA8452Q_init();						// Initialize accelerometer

		// Set LCD text from contents of RAM vars
		ST7032_UpdateText();
	}

	// Loop until enough time has passed without an accelerometer interrupt.
	// Note: the PIO 0.16 pin is configured by default as GPIO, digital mode, input so no configuration is needed.
	SysTick_Reset();	// Reset systick counter
	while( SysTick_Value < BACKLIGHT_DELAY || (LPC_GPIO->B0[ 3 ] == 1) )
	{
		set_led( LPC_GPIO->B0[ 3 ] );

		// Has an accelerometer interrupt occurred?
		if( LPC_GPIO->B0[ 16 ] == 0 )
		{
			// Yes: reset timeout counter and read accelerometer register to clear interrupt
			SysTick_Reset();
//			MMA8452Q_read_register( FF_MT_SRC );
			MMA8452Q_read_register( TRANSIENT_SRC );
		}

		// If USB is connected, we'll reset the timeout counter
		if( LPC_GPIO->B0[ 3 ] == 1 )
			SysTick_Reset();

		// USB command received?
		if( state == StateCmdReceived )
			parseCommand();
	}

	// LCD backlight off
	LPC_GPIO->CLR[ 1 ] = (1<< 13);
	SysTick_Delay( 2 );	// Brief delay to allow the MOSFET to turn completely off by draining the gate charge into the pin.

	// Go to sleep
	/* WARNING: Going into deep power-down mode will mess up the SWD interface because the MCU powers down before the debugger can
	 * access the MCU. This will result in a "MEM-AP is not selected" error when trying to flash the MCU.
	 *
	 * If this happens, start the MCU in ISP mode by doing the following:
	 * 1) power off MCU *and* LPC-Link, disconnect the PWR and GND wires from the LPC-Link
	 * 2) connect P0.1 (ISP) to GND
	 * 3) power up MCU – it should now start up in ISP mode with pins in default (i.e. usable) mode
	 * 4) connect LPC-Link to power and GND
	 * 5) connect LPC-Link to USB
	 * 6) Flash ("Debug")
	 * 7) When log says done writing to flash, disconnect P0.1 and debugging should commence. If not, power off MCU and LPC-Link and disconnect LPC-Link from USB, then startup normally.
	 */

	set_led( 0 );
	deep_power_down();	// NB: we don't care what happens after going into deep power-down, since the MCU restarts from the beginning when waking up instead of resuming.

	// To keep the compiler happy (or when deep power-down is disabled)
	while( 1 )
		;

	return 0;
}

/**
 * Parses the contents of the command buffer and performs appropriate actions.
 */
void parseCommand()
{
	uint8_t i, r;
	char tmpBuffer[17];	// Temporary buffer for changing LCD text

	if( commandBuffer[ 0 ] == 'W' )
	{
		// write command: lines 1 and 2 are separated by a newline character
		// Line 1
		for( i = 1, r = 0; commandBuffer[ i ] && commandBuffer[ i ] != '\n'; i++, r++ )
			tmpBuffer[ r ] = commandBuffer[ i ];

		tmpBuffer[ r ] = 0;			// Terminate string
		ST7032_WriteLine1( tmpBuffer );	// Update

		// Line 2
		i++;	// Skip newline
		for( r = 0; commandBuffer[ i ] && commandBuffer[ i ] != '\r'; i++, r++ )
			tmpBuffer[ r ] = commandBuffer[ i ];

		tmpBuffer[ r ] = 0;				// Terminate string
		ST7032_WriteLine2( tmpBuffer );	// Update

		// Update EEPROM. line1 and line2 has been updated by the ST7032_WriteLineX functions.
		writeEEPROM( (uint8_t*)1, (uint8_t*)line1, 16 );
		writeEEPROM( (uint8_t*)17, (uint8_t*)line2, 16 );

		USB_CDC_print( "# Updated" );
	}
	else if( commandBuffer[ 0 ] == 'R' )
	{
		// read command: return line 1 and 2 separated by newline
		for( i = 0, r = 0; line1[ i ] && i < 16; i++, r++ )
			sendBuffer[ r ] = line1[ i ];

		sendBuffer[ r ] = '\n';
		r++;

		for( i = 0; line1[ i ] && i < 16; i++, r++ )
			sendBuffer[ r ] = line2[ i ];

		sendBuffer[ r ] = 0;	// Terminate string

		USB_CDC_print( (char*)sendBuffer );
	}
	else
	{
		// Unknown command
		USB_CDC_print( "# ERROR unknown command" );
	}

	// Reset command buffer and state
	commandBufferPtr = 0;
	state = StateIdle;
}

/**
 * Utility function for printing messages to USB CDC.
 *
 * @param 	string	The string to print
 */
void USB_CDC_print( char* string )
{
	USB_CDC_send( (uint8_t*)string, strlen( string ));
}

/**
 * This method gets called when data is received on the USB CDC connection.
 *
 * @param 	bufferPtr	Pointer to the data buffer
 * @param 	length		Number of bytes received
 */
void USB_CDC_receive( uint8_t *bufferPtr, uint32_t length )
{
	// Copy into command buffer
	// Prevent buffer overflow
	if( commandBufferPtr + length > CMD_BUF_SIZE )
	{
		// Overflow: ignore this entire chunk (since it will be invalid anyway) and send error
		USB_CDC_print( "# ERROR command buffer overflow!" );

		// Reset command buffer
		commandBufferPtr = 0;
		return;
	}

	// No overflow: copy data into command buffer
	memcpy( commandBuffer+commandBufferPtr, bufferPtr, length );
	commandBufferPtr += length;

	// Check if we have received \r to terminate command
	if( commandBuffer[ commandBufferPtr-1 ] == '\r' )
		// Yes, we have: parse the command
		state = StateCmdReceived;

	// Data received: echo
//	USB_CDC_send( bufferPtr, length );
}

