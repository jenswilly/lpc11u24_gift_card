/*
 * status_leds.c
 *
 *  Created on: Sep 27, 2012
 *      Author: jenswilly
 */

#include "LPC11Uxx.h"
#include "status_led.h"
#include "drivers/systick.h"

/**
 * Initializes PIO0_2 debug LED.
 */
void init_leds()
{
	// Debug LED: PIO0_2
	LPC_IOCON->PIO0_2 = 0x80;		// GPIO
	LPC_GPIO->DIR[0] |= (1<< 2);	// PIO0_2 as output
}

/**
 * Turns the debug LED on or off.
 *
 * @param	on	1 to turn on; 0 to turn off
 */
void set_led( uint8_t on )
{
	if( on )
		LPC_GPIO->SET[ 0 ] = (1<< 2);
	else
		LPC_GPIO->CLR[ 0 ] = (1<< 2);
}

/**
 * Flashes the debug LED the specified number of times with an interval between.
 *
 * @note	This method runs synchronously and uses SysTick_Delay.
 *
 * @note	SysTick driver must be initialized with a 1 ms tick.
 *
 * @param flashes	The number of times to flash
 * @param delayMs	Time in ms for each on and off duration.
 */
void flash_leds( uint8_t flashes, uint16_t delayMs )
{
	uint8_t i;
	for( i=0; i < flashes; i++ )
	{
		// On
		set_led( 1 );
		SysTick_Delay( delayMs );

		// Off
		set_led( 0 );
		SysTick_Delay( delayMs );
	}
}
