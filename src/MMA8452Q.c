/*
 * MMA8452Q.c
 *
 *  Created on: Jan 15, 2014
 *      Author: jenswilly
 */

#include "LPC11Uxx.h"
#include "MMA8452Q.h"
#include "drivers/i2c.h"

#define MOTION		0	// Set to 1 to use motion interrupts
#define TRANSIENT	1	// Set to 1 to use transient interrupts. This will enable the high-pass filter which eliminates static accelerations (gravity)

void MMA8452Q_init()
{
	uint8_t ctrl_reg1;

	// Put device in standby mode
	MMA8452Q_write_register( CTRL_REG1, 0 );

	// Configure for 2 g, no high-pass filter
	MMA8452Q_write_register( XYZ_DATA_CFG, 0x00 );

	// Configure data rate for 50 Hz asleep, 100 Hz awake (standby)
	// b0001 1000
	ctrl_reg1 = 0x18;
	MMA8452Q_write_register( CTRL_REG1, ctrl_reg1 );

#if MOTION
	// Configure motion/free-fall: ELE = 1 (latch enabled, for auto-reset when reading xx), OAE = 1 (motion detection), XEFE, YEFE, ZEFE = 1 (all axes sampled)
	// b1111 1000
	MMA8452Q_write_register( FF_MT_CFG, 0xF8 );	// X and Y axes only. Was 0xF8 (all axes)

	// Configure motion/free-fall threshold: debounce counter mode=0 (inc/dec), threshold = ~0.5 g (in 0.063 g steps)
	// b0000 1000
	MMA8452Q_write_register( FF_MT_THS, 17 );	// was:0x04

	// Configure required debounce count: 10. Time step af ODR=100 Hz, normal mode is 10 ms so required time = 100 ms = 0.1 s.
	// b0000 1010
	MMA8452Q_write_register( FF_MT_COUNT, 2 );	// was:0x0A

	// Motion/free-fall interrupt enabled
	MMA8452Q_write_register( CTRL_REG4, 0x04 );
#endif
#if TRANSIENT
	MMA8452Q_write_register( HP_FILTER_CUTOFF, 0x03);	// Cutoff freq. = 0.5 Hz
	MMA8452Q_write_register( TRANSIENT_CFG, 0x1E );		// HPF, all axes
	MMA8452Q_write_register( TRANSIENT_THS, 0x04 );		// Threshold in 0.063 g
	MMA8452Q_write_register( TRANSIENT_COUNT, 2 );		// Debounce count = 2
	MMA8452Q_write_register( CTRL_REG4, 0x20 );			// Enable transient interrupts
#endif

	// Configure interrupts as active low, push-pull
	MMA8452Q_write_register( CTRL_REG3, 0x00 );

	// All interrupts routed to INT2 pin
	MMA8452Q_write_register( CTRL_REG5, 0x00 );

	// Active
	MMA8452Q_write_register( CTRL_REG1, ctrl_reg1 | BIT_ACTIVE );

}

uint8_t MMA8452Q_read_register( uint8_t register_address )
{
	I2CWriteLength = 2;
	I2CReadLength = 1;
	I2CMasterBuffer[ 0 ] = MMA8452Q_ADDRESS | WRITE_BIT;
	I2CMasterBuffer[ 1 ] = register_address;
	I2CMasterBuffer[ 2 ] = MMA8452Q_ADDRESS | READ_BIT;
	I2CEngine();

	// Get read byte and return it
	return I2CSlaveBuffer[ 0 ];
}

void MMA8452Q_write_register( uint8_t register_address, uint8_t value )
{
	I2CWriteLength = 3;
	I2CReadLength = 0;
	I2CMasterBuffer[ 0 ] = MMA8452Q_ADDRESS | WRITE_BIT;
	I2CMasterBuffer[ 1 ] = register_address;
	I2CMasterBuffer[ 2 ] = value;
	I2CEngine();
}

