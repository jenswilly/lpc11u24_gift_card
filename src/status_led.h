/*
 * status_leds.h
 *
 *  Created on: Sep 27, 2012
 *      Author: jenswilly
 */

#ifndef STATUS_LEDS_H_
#define STATUS_LEDS_H_

void init_leds(void);
void set_led( uint8_t on );
void flash_leds( uint8_t flashes, uint16_t delayMs );

#endif /* STATUS_LEDS_H_ */
