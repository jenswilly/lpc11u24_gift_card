/*
 * sleep.h
 *
 *  Created on: Jan 13, 2014
 *      Author: jenswilly
 */

#ifndef SLEEP_H_
#define SLEEP_H_

void deep_power_down( void );

#endif /* SLEEP_H_ */
