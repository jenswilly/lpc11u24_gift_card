/*
 * LCD.c
 *
 *  Created on: Jan 11, 2014
 *      Author: jenswilly
 *
 *  Driver for MCCOG21605C6W-BNMLWI LCD display based on a ST7032i I2C controller.
 */

#include "LPC11Uxx.h"
#include "LCD.h"
#include "drivers/i2c.h"
#include "drivers/systick.h"
#include <string.h>

// Buffers for line 1 and 2 – we don't use sideways scrolling so only 16 characters per line
char line1[ 16 ];
char line2[ 16 ];

/**
 * Initializes the LCD driver.
 * The I2C pins and driver must already be initialized.
 * SysTick driver must also be initalized and configured for 1 ms ticks.
 *
 * @param initMode	The initialization sequence to use.
 */
void ST7032_init( ST7032_InitMode initMode )
{
	// Which initialization mode?
	switch( initMode )
	{
	case ST7032_InitMode_3V:
		// Not yet implemented
		break;

	case ST7032_InitMode_3V3:
		/* From http://www.swarthmore.edu/NatSci/echeeve1/Ref/embedRes/DL/NHD-C0216CZ-FSW-FBW-3V3.pdf
		 *
		 * Writecom(0x30);
		 * delay(2);
		 * Call writecom(0x30);
		 * Call writecom(0x30);
		 * Call writecom(0x39);
		 * Call writecom(0x14);
		 * Call writecom(0x56);
		 * Call writecom(0x6D);
		 * Call writecom(0x70);
		 * Call writecom(0x0C);
		 * Call writecom(0x06);
		 * Call writecom(0x01);
		 * delay(10);
		 */

		I2CReadLength = 0;
		I2CWriteLength = 3;
		I2CMasterBuffer[0] = ST7032_ADDRESS | WRITE_BIT;
		I2CMasterBuffer[1] = ST7032_COMMAND_WRITE;
		I2CMasterBuffer[2] = 0x30;	// "Wake-up"
		I2CEngine( );
		SysTick_Delay( 1 );			// Delay

		I2CMasterBuffer[2] = 0x39;	// Function set: 2 lines, instruction set table 1
		I2CEngine( );
		SysTick_Delay( 1 );			// Min. 27 µs

		I2CWriteLength = 9;
		I2CMasterBuffer[ 2 ] = 0x14;	// Adjust OSC freq.
		I2CMasterBuffer[ 3 ] = 0x56;	// Power/ICON/Contrast control
		I2CMasterBuffer[ 4 ] = 0x6D;	// Follower control
		I2CMasterBuffer[ 5 ] = 0x70;	// Contrast set
		I2CMasterBuffer[ 6 ] = 0x0C;	// DISPLAY ON, cursor off
		I2CMasterBuffer[ 7 ] = 0x06;	// ENTRY MODE SET
		I2CMasterBuffer[ 8 ] = 0x01;	// CLEAR DISPLAY, set DDRAM address to 0x00
		I2CEngine( );
		SysTick_Delay( 1 );				// Delay
		break;

	case ST7032_InitMode_3V3_v2:
		/* From http://snipt.org/AaLh7
		 * //CONTRAST = 0x28
		 * delay_ms(100);
		 * sendLCD(&transferMCfg,0x38);				// 8bit 2line Normal mode
		 * sendLCD(&transferMCfg,0x39);				// 8bit 2line Extend mode
		 * sendLCD(&transferMCfg,0x14);				// OSC 183Hz BIAS 1/5
		 *
		 * sendLCD(&transferMCfg, 0x70 + (CONTRAST & 0x0F));	// 0x78
		 * sendLCD(&transferMCfg,0x5C + (CONTRAST >> 4));		// 0x5E
		 * //sendLCD(&transferMCfg,0x6A);				// Follower for 5.0V
		 * sendLCD(&transferMCfg,0x6B);				// Follower for 3.3V
		 * delay_ms(300);
		 * sendLCD(&transferMCfg,0x38);				// Set Normal mode
		 * sendLCD(&transferMCfg,0x0C);				// Display On
		 * sendLCD(&transferMCfg,0x01);
		 */
		I2CReadLength = 0;
		I2CWriteLength = 3;
		I2CMasterBuffer[0] = ST7032_ADDRESS | WRITE_BIT;
		I2CMasterBuffer[1] = ST7032_COMMAND_WRITE;
		I2CMasterBuffer[2] = 0x38;	// Instruction set
		I2CEngine( );
		SysTick_Delay( 1 );			// Delay

		I2CMasterBuffer[2] = 0x39;	// Function set: 2 lines, instruction set table 1
		I2CEngine( );
		SysTick_Delay( 1 );			// Min. 30 µs

		I2CWriteLength = 6;
		I2CMasterBuffer[ 2 ] = 0x14;	// Adjust OSC freq.
		I2CMasterBuffer[ 3 ] = 0x78;	// Contrast set
		I2CMasterBuffer[ 4 ] = 0x5E;	// Power/ICON/Contrast control
		I2CMasterBuffer[ 5 ] = 0x6B;	// Follower control, 3.3V
		SysTick_Delay( 300 );
		I2CEngine( );

		I2CWriteLength = 5;
		I2CMasterBuffer[ 2 ] = 0x38;	// Instruction set
		I2CMasterBuffer[ 3 ] = 0x0C;	// DISPLAY ON, cursor off
		I2CMasterBuffer[ 4 ] = 0x01;	// CLEAR DISPLAY, set DDRAM address to 0x00
		I2CEngine( );
		SysTick_Delay( 1 );				// Delay
		break;

	case ST7032_InitMode_5V:
	/*
		MOV I2C_CONTROL,#00H	;WRITE COMMAND
		MOV I2C_DATA,#38H		;Function Set
		LCALL WRITE_CODE
		MOV I2C_CONTROL,#00H	;WRITE COMMAND
		MOV I2C_DATA,#39H		;Function Set
		LCALL WRITE_CODE
		MOV I2C_DATA,#14H		;Internal OSC frequency
		LCALL WRITE_CODE
		MOV I2C_DATA,#79H		;Contrast set
		LCALL WRITE_CODE
		MOV I2C_DATA,#50H		;Power/ICON control/Contrast set
		LCALL WRITE_CODE
		MOV I2C_DATA,#6CH		;Follower control
		LCALL WRITE_CODE
		MOV I2C_DATA,#0CH		;DisplayON/OFF
		LCALL WRITE_CODE
		MOV I2C_DATA,#01H		;Clear Display
		LCALL WRITE_CODE
	 */

		I2CReadLength = 0;
		I2CWriteLength = 3;
		I2CMasterBuffer[0] = ST7032_ADDRESS | WRITE_BIT;
		I2CMasterBuffer[1] = ST7032_COMMAND_WRITE;
		I2CMasterBuffer[2] = 0x38;	// Function set: 2 lines
		I2CEngine( );
		SysTick_Delay( 1 );			// Min. 27 µs

		I2CMasterBuffer[2] = 0x39;	// Function set: 2 lines, instruction set table 1
		I2CEngine( );
		SysTick_Delay( 1 );			// Min. 27 µs

		I2CWriteLength = 9;
		I2CMasterBuffer[ 2 ] = 0x14;	// Adjust OSC freq. (192 Hz @ 5 V)
		I2CMasterBuffer[ 3 ] = 0x79;	// Contrast set
		I2CMasterBuffer[ 4 ] = 0x50;	// Power/ICON/Contrast control
		I2CMasterBuffer[ 5 ] = 0x6C;	// Follower control
		I2CMasterBuffer[ 6 ] = 0x0C;	// DISPLAY ON, cursor off
		I2CMasterBuffer[ 7 ] = 0x01;	// CLEAR DISPLAY, set DDRAM address to 0x00
		I2CMasterBuffer[ 8 ] = 0x06;	// ENTRY MODE SET
		I2CEngine( );
		SysTick_Delay( 1 );			// Min. 27 µs
		break;
	}
}

/**
 * Updates line 1 of the display. Text is updated immediately.
 *
 * @param text	A string containing a maximum of 16 characters.
 */
void ST7032_WriteLine1( const char* text )
{
	uint8_t i=0;

	// Copy text into line buffer (max. 16 characters)
	while( i <= 15 && text[ i ] )
	{
		line1[ i ] = text[ i ];
		i++;
	}

	// Pad with spaces if necessary
	while( i <= 15  )
		line1[ i++ ] = 0x20;

	// Update display. Set DDRAM address to 0x00
	I2CReadLength = 0;
	I2CWriteLength = 3;
	I2CMasterBuffer[0] = ST7032_ADDRESS | WRITE_BIT;
	I2CMasterBuffer[1] = ST7032_COMMAND_WRITE;
	I2CMasterBuffer[2] = 0x80;	// DDRAM address = 0x00
	I2CEngine( );

	// Write 16 bytes of data to DDRAM
	I2CWriteLength = 18;
	I2CMasterBuffer[ 0 ] = ST7032_ADDRESS | WRITE_BIT;
	I2CMasterBuffer[ 1 ] = ST7032_DATA_WRITE;
	memcpy( (void*)I2CMasterBuffer+2, line1, 16 );		// Copy line1 data into I2C master buffer at offset 2
	I2CEngine( );
}

/**
 * Updates line 2 of the display. Text is updated immediately.
 *
 * @param text	A string containing a maximum of 16 characters.
 */
void ST7032_WriteLine2( const char* text )
{
	uint8_t i=0;

	// Copy text into line buffer (max. 16 characters)
	while( i <= 15 && text[ i ] )
	{
		line2[ i ] = text[ i ];
		i++;
	}

	// Pad with spaces if necessary
	while( i <= 15  )
		line2[ i++ ] = 0x20;

	// Update display. Set DDRAM address to 0x40
	I2CReadLength = 0;
	I2CWriteLength = 3;
	I2CMasterBuffer[0] = ST7032_ADDRESS | WRITE_BIT;
	I2CMasterBuffer[1] = ST7032_COMMAND_WRITE;
	I2CMasterBuffer[2] = 0xC0;	// DDRAM address = 0x40
	I2CEngine( );

	// Write 16 bytes of data to DDRAM
	I2CWriteLength = 18;
	I2CMasterBuffer[ 0 ] = ST7032_ADDRESS | WRITE_BIT;
	I2CMasterBuffer[ 1 ] = ST7032_DATA_WRITE;
	memcpy( (void*)I2CMasterBuffer+2, line2, 16 );		// Copy line2 data into I2C master buffer at offset 2
	I2CEngine( );
}

void ST7032_UpdateText()
{
	// Line 1
	// Update display. Set DDRAM address to 0x00
	I2CReadLength = 0;
	I2CWriteLength = 3;
	I2CMasterBuffer[0] = ST7032_ADDRESS | WRITE_BIT;
	I2CMasterBuffer[1] = ST7032_COMMAND_WRITE;
	I2CMasterBuffer[2] = 0x80;	// DDRAM address = 0x00
	I2CEngine( );

	// Write 16 bytes of data to DDRAM
	I2CWriteLength = 18;
	I2CMasterBuffer[ 0 ] = ST7032_ADDRESS | WRITE_BIT;
	I2CMasterBuffer[ 1 ] = ST7032_DATA_WRITE;
	memcpy( (void*)I2CMasterBuffer+2, line1, 16 );		// Copy line1 data into I2C master buffer at offset 2
	I2CEngine( );

	// Line 2
	// Update display. Set DDRAM address to 0x40
	I2CReadLength = 0;
	I2CWriteLength = 3;
	I2CMasterBuffer[0] = ST7032_ADDRESS | WRITE_BIT;
	I2CMasterBuffer[1] = ST7032_COMMAND_WRITE;
	I2CMasterBuffer[2] = 0xC0;	// DDRAM address = 0x40
	I2CEngine( );

	// Write 16 bytes of data to DDRAM
	I2CWriteLength = 18;
	I2CMasterBuffer[ 0 ] = ST7032_ADDRESS | WRITE_BIT;
	I2CMasterBuffer[ 1 ] = ST7032_DATA_WRITE;
	memcpy( (void*)I2CMasterBuffer+2, line2, 16 );		// Copy line2 data into I2C master buffer at offset 2
	I2CEngine( );

}

