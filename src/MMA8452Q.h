/*
 * MMA8452Q.h
 *
 *  Created on: Jan 15, 2014
 *      Author: jenswilly
 */

#ifndef MMA8452Q_H_
#define MMA8452Q_H_

#define MMA8452Q_ADDRESS 0x38	// Address if SA0 pin is grounded. W/o read bit set.
//#define MMA8452Q_ADDRESS 0x3A	// Address if SA0 pin is pulled up. W/o read bit set.

// Register and bit definitions
#define CTRL_REG1			0x2A
#define CTRL_REG3			0x2C
#define CTRL_REG4			0x2D
#define CTRL_REG5			0x2E
#define INT_SOURCE			0x0C
#define XYZ_DATA_CFG		0x0E
#define HP_FILTER_CUTOFF	0x0F
#define FF_MT_CFG			0x15
#define	FF_MT_SRC			0x16
#define FF_MT_THS			0x17
#define FF_MT_COUNT			0x18

#define TRANSIENT_CFG		0x1D
#define TRANSIENT_SRC		0x1E
#define TRANSIENT_THS		0x1F
#define TRANSIENT_COUNT 	0x20

#define BIT_ACTIVE	1
#define BITS_2G 	0x00
#define BITS_4G 	0x01
#define BITS_8G 	0x10

// Function prototypes
void MMA8452Q_init( void );
uint8_t MMA8452Q_read_register( uint8_t register_address );
void MMA8452Q_write_register( uint8_t register_address, uint8_t value );

#endif /* MMA8452Q_H_ */
