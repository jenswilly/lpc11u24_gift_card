/*
 * LCD.h
 *
 *  Created on: Jan 11, 2014
 *      Author: jenswilly
 */

#ifndef LCD_H_
#define LCD_H_

#define ST7032_ADDRESS 0x7c	// I2C address byte. Specify as 8 bit address w/o R bit set.
#define ST7032_COMMAND_WRITE 0x00	// Initial byte to send for commands
#define ST7032_DATA_WRITE 0x40		// Initial byte to send for data commands

typedef enum
{
	ST7032_InitMode_5V,
	ST7032_InitMode_3V3,	// From http://www.swarthmore.edu/NatSci/echeeve1/Ref/embedRes/DL/NHD-C0216CZ-FSW-FBW-3V3.pdf
	ST7032_InitMode_3V3_v2,	// From http://snipt.org/AaLh7
	ST7032_InitMode_3V
} ST7032_InitMode;

extern char line1[ 16 ];
extern char line2[ 16 ];

// Function prototypes
void ST7032_init( ST7032_InitMode initMode );
void ST7032_WriteLine1( const char* text );
void ST7032_WriteLine2( const char* text );
void ST7032_UpdateText( void );

/* For Danish characters use the following values in strings:
 * æ	\x91
 * ø	\xEF
 * å	\x86
 *
 * Æ	\x92
 * Ø	\xEE
 * Å	\x8F
 */

#endif /* LCD_H_ */
